/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package md5;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class md5 {
    public static String abrirTXT(){
        String ruta="";
        String nombre="";
        JFileChooser selector=new JFileChooser();
        selector.setDialogTitle("Seleccione una archivo");
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("TXT","txt");
        selector.setFileFilter(filtro);
        int flag=selector.showOpenDialog(null);
        if(flag==JFileChooser.APPROVE_OPTION){
            try {
                File Seleccion=selector.getSelectedFile();
                ruta=Seleccion.getPath();
                nombre=Seleccion.getName();
                principal.jTextArea1.setText("");
                principal.jLabel1.setText("file: "+nombre);
                try {
                    llenaTextArea(ruta);
                } catch (IOException ex) {
                    Logger.getLogger(md5.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (Exception e) {
            }
                 
        }
        return ruta;
    }
    
    private static void llenaTextArea(String ruta)throws FileNotFoundException, IOException {
        String cadena;
      FileReader f = new FileReader(ruta);
      BufferedReader b = new BufferedReader(f);
      while((cadena = b.readLine())!=null) {
          //System.out.println(cadena);
          principal.jTextArea1.append(cadena+"\n");
      }
      b.close();
    }
    
    public static void guardarTXT(String texto){
        //Creamos el objeto JFileChooser
        JFileChooser fc=new JFileChooser();
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("TXT","txt");
        fc.setFileFilter(filtro);
        //Abrimos la ventana, guardamos la opcion seleccionada por el usuario
        int seleccion=fc.showSaveDialog(null);
        //Si el usuario, pincha en aceptar
        if(seleccion==JFileChooser.APPROVE_OPTION){
            //Seleccionamos el fichero
            File fichero=fc.getSelectedFile();
            try(
                FileWriter fw=new FileWriter(fichero+".txt")){
                //Escribimos el texto en el fichero
                fw.write(texto);
                fw.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }
    
    
    public String hashFinction(String texto) throws Exception {
    //String datafile = "c:\\hola.txt"; // descomentar para usar con archivos 

    MessageDigest md = MessageDigest.getInstance("MD5");//SHA1
    //FileInputStream fis = new FileInputStream(datafile); // descomentar para usar con archivos 
    InputStream fis = new ByteArrayInputStream(texto.getBytes());// comentar para usar con cadenas
    byte[] dataBytes = new byte[1024];
    
    int nread = 0; 
    
    while ((nread = fis.read(dataBytes)) != -1) {
      md.update(dataBytes, 0, nread);
    };

    byte[] mdbytes = md.digest();
   
    //convert the byte to hex format
    StringBuffer sb = new StringBuffer("");
    for (int i = 0; i < mdbytes.length; i++) {
    	sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
    }
    
    System.out.println("Digest(in hex format):: " + sb.toString());
    return sb.toString();
    }
}
