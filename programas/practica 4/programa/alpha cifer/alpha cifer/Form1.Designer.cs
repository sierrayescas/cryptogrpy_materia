﻿namespace alpha_cifer
{
    partial class Affine_cipher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.code = new System.Windows.Forms.TextBox();
            this.encoded = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.a = new System.Windows.Forms.NumericUpDown();
            this.b = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(331, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Encrypt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Location = new System.Drawing.Point(331, 168);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Decrypt";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // code
            // 
            this.code.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.code.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.code.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.code.Location = new System.Drawing.Point(12, 12);
            this.code.MaxLength = 50000;
            this.code.Multiline = true;
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(266, 392);
            this.code.TabIndex = 2;
            this.code.TabStop = false;
            this.code.TextChanged += new System.EventHandler(this.code_TextChanged);
            // 
            // encoded
            // 
            this.encoded.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.encoded.Location = new System.Drawing.Point(453, 11);
            this.encoded.MaxLength = 50000;
            this.encoded.Multiline = true;
            this.encoded.Name = "encoded";
            this.encoded.Size = new System.Drawing.Size(269, 393);
            this.encoded.TabIndex = 3;
            this.encoded.TabStop = false;
            this.encoded.TextChanged += new System.EventHandler(this.encoded_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(341, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "a";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(342, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "b";
            // 
            // a
            // 
            this.a.Location = new System.Drawing.Point(341, 41);
            this.a.Name = "a";
            this.a.Size = new System.Drawing.Size(62, 20);
            this.a.TabIndex = 8;
            // 
            // b
            // 
            this.b.Location = new System.Drawing.Point(345, 102);
            this.b.Name = "b";
            this.b.Size = new System.Drawing.Size(61, 20);
            this.b.TabIndex = 9;
            // 
            // Affine_cipher
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(734, 481);
            this.Controls.Add(this.b);
            this.Controls.Add(this.a);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.encoded);
            this.Controls.Add(this.code);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.KeyPreview = true;
            this.Name = "Affine_cipher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Affine cipher";
            this.Load += new System.EventHandler(this.Affine_cipher_Load);
            ((System.ComponentModel.ISupportInitialize)(this.a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox encoded;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown b;
        private System.Windows.Forms.NumericUpDown a;
    }
}

