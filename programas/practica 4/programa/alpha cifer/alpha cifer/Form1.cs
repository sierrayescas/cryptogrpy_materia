﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace alpha_cifer
{
    public partial class Affine_cipher : Form
    {

        public Affine_cipher()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mes;
            int i,espacios,j;
            StreamReader src = new StreamReader("C:\\Users\\sierrayescas\\Downloads\\school+\\ESCOM\\6\\cryptograpy\\programas\\practica 4\\m.txt");
            mes = src.ReadToEnd();
            src.Close();
            code.Text= mes;
            espacios = 0;
            char[] caracteres = new char[mes.Length];
            using (StringReader sr = new StringReader(mes))
            {
                sr.Read(caracteres, 0, mes.Length);
            }
            for(i=0;i<mes.Length; i++)
            {
                if (caracteres[i] == ' '|| (int)caracteres[i]<=96|| (int)caracteres[i]>=123)
                {
                    espacios++;
                }
            }
            char[] mtoc = new char[mes.Length - espacios];
            j = 0;
            for (i = 0; i < mes.Length-espacios; i++)
            {
                while (true)
                {
                    if (caracteres[j] == ' ' || (int)caracteres[j] <= 96 || (int)caracteres[j] >= 123)
                    {
                        j++;
                    }
                    else
                    {
                        break;
                    }
                }
                
                mtoc[i] = caracteres[j];
                j++;
            }
            //llamado a la funcion codificar
            encrypt(mtoc);
            //imprimir resultados
            StreamWriter sw = new StreamWriter("C:\\Users\\sierrayescas\\Downloads\\school+\\ESCOM\\6\\cryptograpy\\programas\\practica 4\\c.txt");
            string codificado = new string(mtoc);
                encoded.Text += codificado;
            sw.WriteLine(codificado);
            sw.Close();
        }

        private void encrypt(char[] message)
        {
            int i;
            for (i = 0; i < message.Length; i++)
            {
                message[i] = (char)((char)message[i] - (char)'a');
                message[i] = (char)(((a.Value * (int)(message[i]))+b.Value)%26) ;
                message[i] = (char)((char)message[i] + (char)'A');
            }


        }
        private void Affine_cipher_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void encoded_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string mes;
            int i;
            StreamReader src = new StreamReader("C:\\Users\\sierrayescas\\Downloads\\school+\\ESCOM\\6\\cryptograpy\\programas\\practica 4\\c.txt");
            mes = src.ReadToEnd();
            src.Close();
            char[] caracteres = new char[mes.Length];
            using (StringReader sr = new StringReader(mes))
            {
                sr.Read(caracteres, 0, mes.Length);
            }
            //llamado a la funcion codificar
            decrypt(caracteres);
            code.Text = ("");
            StreamWriter sw = new StreamWriter("C:\\Users\\sierrayescas\\Downloads\\school+\\ESCOM\\6\\cryptograpy\\programas\\practica 4\\rm.txt");
            string decodificado = new string(caracteres);
                code.Text +=decodificado;
            sw.WriteLine(decodificado);
            sw.Close();
            
        }
        private void decrypt(char[] message)
        {
            int i=0;
            int res, mult;
            int al = (int)a.Value;
            int bl=26;
            int mcd=0;
            int ap;
            //euclides
            do
            {
                mult = (int)(bl / al);
                res = (int)(bl % al);
                bl = al;
                al = res;
                if (res > 0)
                {
                    mcd = res;
                }
                i++;
            } while (res>0);
       //     encoded.Text += ("mcd =" + mcd);
            //extendido
            if (mcd == 1)
            {                                   // m = a(x) + b(y)
                int[] res2 = new int[i - 1];    //b
                int[] mult2 = new int[i - 1];   //a
                int[] res2_m = new int[i - 1];  //m
                int[] mult2_m = new int[i - 1]; //x
                al = (int)a.Value;              //donde m es = 26 en el primer caso
                bl = 26;                        //y es 'a' del anterior en los demas
                for (i = 0; i < res2.Length; i++)
                {
                    mult2[i] = al;
                    mult2_m[i] = (int)(bl / al);
                    res2[i] = (int)(bl % al);
                    res2_m[i] = bl;
                    bl = al;
                    al = res2[i];
                }
                bl = 26;
                al = (int)a.Value;
                  for (i =0;i<res2.Length;i++)  // impresion de las ecuaciones
                    {
            //    encoded.Text += (res2_m[i]+" = " + mult2[i] +"(" + mult2_m[i] +") + "+res2[i]);
                //encoded.Text += (" "+res2_m.Length);
                }
                int[] resul = new int[4];       // impresion de la ecuacion resuelta
                int aux;
                resul[0] =res2_m[res2.Length-1];            //a(m)  //el que no se remplaza
                resul[1] =1;
                resul[2] =mult2[res2.Length-1];            //c[m2] // el que  se remplaza
                resul[3] =mult2_m[res2.Length-1];
               // encoded.Text += ("1 =" + resul[0] + "(" + resul[1] + ") + " + resul[2] + "(" + resul[3] + ")");
                for (i = res2.Length-2; i>=0; i--)
               {
                    aux = resul[1];
                    resul[0] = res2_m[i];
                    resul[1] = resul[3];
                    resul[2] =mult2[i];
                    resul[3] = resul[3]*mult2_m[i]+aux;
                    //encoded.Text += ("1 =" + resul[0] + "(" + resul[1] + ") + " + resul[2] + "(" + resul[3] + ")");
                }
              //  encoded.Text += ("1 =" + resul[0] + "(" + resul[1] + ") + " + resul[2] + "(" + resul[3] + ")");
                if ((resul[3]*resul[2]) - (resul[1] * resul[0]) == 1)
                {
                    ap = resul[3];
                }
                else
                {
                    ap = 26 - resul[3];
                }
                //operacion
                for (i = 0; i < message.Length; i++)
                {       //m = a^-1[c+(b^-1)]mod26
                    message[i] = (char)((char)message[i] - (char)'A');
                    message[i] = (char)((ap *(((int)message[i]) + (26 - (b.Value % 26)))) % 26);
                    message[i] = (char)((char)message[i] + (char)'a');
                }
            }

        }

        private void code_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
