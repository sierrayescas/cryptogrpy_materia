﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Codificar
{
    class Program
    {
        static void Main(string[] args)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            string resultado = string.Empty;
            string cadena;
            cadena = Console.ReadLine();
            byte[] encrypted = Encoding.Unicode.GetBytes(cadena);
            byte[] hash = md5.ComputeHash(encrypted);
            resultado = Convert.ToBase64String(hash);
            Console.WriteLine(resultado);
            Console.ReadKey();
        }


    }
}
