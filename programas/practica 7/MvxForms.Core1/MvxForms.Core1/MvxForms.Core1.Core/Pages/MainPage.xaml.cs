﻿// ---------------------------------------------------------------
// <author>Paul Datsyuk</author>
// <url>https://www.linkedin.com/in/pauldatsyuk/</url>
// ---------------------------------------------------------------

using MvvmCross.Forms.Views;
using MvxForms.Core1.Core.ViewModels;

namespace MvxForms.Core1.Core.Pages
{
    public partial class MainPage : MvxContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
